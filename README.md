# [RIPSters](https://gitlab.com/zeen3/ripsters/)

Various site ripping scripts; tries to be high consistency.

Requires greasemonkey or tampermonkey.

Files are in `public/` because it lets me [host them](https://zeen3.gitlab.io/ripsters/) easier.

Scripts are tested in few situations; and generally only on chromium:latest using tampermonkey.

Some scripts are more necessary than others (i.e.: can easily grab files off the site) but perform useful functions, such as slicing to a notable break in the image.

Please contribute your own.

## Usage restrictions

There are no primary usage restrictions. There is a request that you contribute raw providers to [`Fa Q Scans`/#2978](https://mangadex.org/group/2978/fa-q-scans); but there are no ways of resolving who's used it.

Thank you.


