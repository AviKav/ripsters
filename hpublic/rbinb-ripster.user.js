// ==UserScript==
// @name         rbinb-ripster
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  rips images from r.binb.jp/epm/*, booklive.jp/bviewer/s/*, binb.bricks.pub/contents/*
// @author       zeen3
// @require      https://zeen3.gitlab.io/ripsters/ripster-shared.js
// @match        https://r.binb.jp/epm/*
// @match        https://booklive.jp/bviewer/s/*
// @match        https://binb.bricks.pub/contents/*
// @grant        GM_notification
// @run-at       document-start
// ==/UserScript==

;(async () => {
	GM_notification({
		title: 'Waiting for load...',
		text: 'rbinb ripster waiting for load'
	})
	const nt = window[window['next-tick']].suicide()
	let start_wait
	await new Promise(r => addEventListener('load', r, {once: true}))
	do {
		start_wait = document.getElementById('start_wait')
		await nt.ric()
	} while (start_wait);
	await new Promise(r => {
		GM_notification({
			title: 'RIPSter ready',
			text: 'rbinb ripster ready, click to start',
			onclick: r,
			timeout: 0
		})
		console.log({get click_to_start(){return r()}})
	})
	console.log('ripster running')
	const moveLeft = new KeyboardEvent('keydown', {keyCode: 37}),
		  moveRight = new KeyboardEvent('keydown', {keyCode: 39}),
		  moveNext = document.body.classList.contains('rtl') ? moveLeft : moveRight,
		  movePrev = document.body.classList.contains('ltr') ? moveLeft : moveRight,
		  sliderv = document.getElementById('menu_slidercaption'),
		  getSliderv = () => parseInt(sliderv.textContent.slice(0, sliderv.textContent.indexOf('/')), 10),
		  getSlivervM = () => parseInt(sliderv.textContent.slice(sliderv.textContent.indexOf('/') + 1)),
		  { __downloadFile__: dl } = nt
	if (document.body.classList.contains('sd_vert')) document.getElementById('menu_change_vh').click()

	while (getSliderv() > 1) {
		document.dispatchEvent(movePrev)
		await nt.delay(666)
	}
	const _imgs = document.querySelectorAll('#content div[data-ptimg][id^="content-p"]')
	const blobs = new Array(_imgs.length)
	const startDL = async () => {
		await dl(new Blob([''], {type: 'text/plain'}), 'placement.txt')
		for (let i = 0; i < _imgs.length; ++i) {
			while (!blobs[i]) await nt.praf()
			await dl(blobs[i], (i + 1).toString(10).padStart(4, '0').concat('.png'))
		}
	}
	let __res, __rej;
	const awaited = new Promise((res, rej) => {__res = res; __rej = rej})
	const isalltrue = truthy => truthy;
	const dlprog = v => bb => {
		blobs[v - 1] = bb
		GM_notification({
			text: `Downloaded ${v}/${_imgs.length}; click to start downloading`,
			title: 'Pre-download progress',
			timeout: 1,
			onclick: startDL
		})
		// hack to ensure every place is filled
		if (Array.from(blobs, v => v instanceof Blob).every(isalltrue)) __res()
		return bb
	}
	for (const imgs of _imgs) {
		const v = parseInt(imgs.id.slice(9), 10)
		console.log('%d/%d', v, _imgs.length)
		while (v - getSliderv() > 2) {
			document.dispatchEvent(moveNext)
			await nt.delay(666)
		}
		while (imgs.querySelector('img') === null) await nt.praf()
		const ppcsm = imgs.computedStyleMap(), ipcsm = imgs.querySelector('img').computedStyleMap()
		const ppw = Math.round(ppcsm.get('width').value / ipcsm.get('transform').toMatrix().a)
		const pph = Math.round(ppcsm.get('height').value / ipcsm.get('transform').toMatrix().d)
		// console.log({ppw, pph})
		const ctx = new OffscreenCanvas(ppw, pph).getContext('2d')
		let _i = imgs.querySelectorAll('img')
		for (const i of _i) {
			// console.log(i)
			await nt.iloadprom(i)
			let im = i.computedStyleMap().get('transform').toMatrix(),
				pm = i.parentElement.computedStyleMap().get('transform').toMatrix()

			ctx.drawImage(i, 0, Math.round(pm.f / im.a))
		}
		ctx.canvas.convertToBlob().then(dlprog(v))
	}
	await awaited
	GM_notification({
		text: `Downloaded ${blobs.length} images; click to download all.`,
		title: `rbinb download complete`,
		timeout: 0,
		onclick: startDL
	})
	console.log({get start_download() {return startDL()}})
	return blobs
})().then(console.log, console.error);
