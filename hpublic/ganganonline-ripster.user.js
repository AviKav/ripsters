// ==UserScript==
// @name         ganganonline-ripster
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  rips images from gangan online
// @author       zeen3
// @match        http://www.ganganonline.com/viewer/player/viewer.html?*
// @connect      ganganonline.com
// @connect      *.ganganonline.com
// @grant        GM_xmlhttpRequest
// @grant        GM_notification
// @run-at       document-end
// @require      https://zeen3.gitlab.io/ripsters/ripster-shared.js
// ==/UserScript==
try {
	const { __downloadFile__: dl, praf, ric, delay, nt } = self[self['next-tick']].suicide()
	const wmr = new WeakMap();
	const wm = new WeakMap();
	const fns = new WeakMap();
	const fnsr = new WeakMap();
	const consts = new WeakMap()
	const constsr = new WeakMap();
	const IMGS = new Map();
	const BMS = new WeakMap();
	const _OSCS = new WeakMap();
	const BLOBS = new Map();
	const BOOKS = new Set();
	const PAGES = [];
	let url = '', dir = 'rtl', pages = 0, bookjson = '', renderer = null, pageSliderCounter = null
	const wms = {wmr, wm, fns, fnsr, consts, constsr,
				 IMGS, BMS, _OSCS, BLOBS, BOOKS, PAGES,
				 get info(){return {url, dir, pages, renderer}}}
	let applyCount = 0, constructCount = 0, tMo = 0
	const getHeaders = ({responseHeaders}) => new Headers(
		responseHeaders.split(/\r?\n/g).filter(v=>v.length).map(a => [
			a.slice(0, a.indexOf(':')),
			a.slice(a.indexOf(':') + 1).trim()
		]))
	const setImg = (src, tO = tMo++ * 100) => src && IMGS.set(src, new Promise(r => setTimeout(() => GM_xmlhttpRequest({
		url: src,
		method: 'GET',
		responseType: 'arraybuffer',
		async onload(resp) {
			let h = getHeaders(resp)
			const bm = await createImageBitmap(new Blob([resp.response], {type: h.get('content-type')}))
			r(bm)
			renderer || (renderer = document.getElementById('renderer'))
			pageSliderCounter || (pageSliderCounter = document.getElementById('pageSliderCounter'))
		}
	}), tO))).get(src)
	const dlAll = async () => {
		await dl(new File([location.search, '\n', bookjson], 'placement.txt', {type: 'text/plain'}))
		let i = 0
		renderer || (renderer = document.getElementById('renderer'))
		pageSliderCounter || (pageSliderCounter = document.getElementById('pageSliderCounter'))
		const next = [new CustomEvent('keydown', {bubbles: true, detail: dir !== 'ltr' ? 37 : 39}), new CustomEvent('keyup', {bubbles: true, detail: dir !== 'ltr' ? 37 : 39})]
		const prev = [new CustomEvent('keydown', {bubbles: true, detail: dir !== 'rtl' ? 37 : 39}), new CustomEvent('keyup', {bubbles: true, detail: dir !== 'rtl' ? 37 : 39})]
		for (const a of [next, prev]) for (const e of a) Object.defineProperties(e, {which: {value: e.detail, enumerable: true}, })
		console.log([...next, ...prev])
		while (i < pages) {
			if (!(await BLOBS.get(url + PAGES[i]))) {
				const page = parseInt(pageSliderCounter.textContent, 10)
				if (page < i) {
					console.log('move %O: %O; %s, %f, %s (%i -> %i)', next, renderer,
								renderer.dispatchEvent(next[0]),
								await praf(),
								renderer.dispatchEvent(next[1]),
								page, i)
				} else {
					console.log('move %O: %O; %s, %f, %s, (%i -> %i)', next, renderer,
								renderer.dispatchEvent(prev[0]),
								await praf(),
								renderer.dispatchEvent(prev[1]),
								page, i)
				}
				await delay(1000)
			} else {
				await dl(BLOBS.get(url + PAGES[i++]), i.toString(10).padStart(4,'0') + '.png')
			}
		}
	}
	const prox = (...w) => {
		const W = w.join('.')
		return {
			get(self, prop) {
				const X = Reflect.get(...arguments)
				const T = typeof X
				//X && T === 'object' && 'destX' in X && console.debug('GET %s.%s => %O', W, prop, X)
				const prx = wm.get(X) ||
					  ((('object' === T && X !== null && !(
						  X instanceof Image ||
						  X instanceof HTMLCanvasElement ||
						  X instanceof CanvasRenderingContext2D))
						|| 'function' === T
					   ) ? new Proxy(X, prox(...w, prop)) : null)
				return prx === null ? X : wm.get(X) || (wm.set(X, prx).set(prx, prx), wmr.set(prx, X), prx)
			},
			apply(fn, self, args) {
				const R = Reflect.apply(...arguments)
				if ('object' === typeof R && R !== null && !(R instanceof Image || R instanceof HTMLCanvasElement)) {
					const prx = new Proxy(R, prox(...w, `(APPLY#${++applyCount})`))
					//console[W === 'NFBR.a3E.a3f' ? 'debug' : 'info'](
					//	`APPLY#%d %s <%O>(${new Array(args.length).fill('%O').join(', ')}) => %O`,
					//	applyCount, W, fn, ...args, R)
					fns.set(R, prx).set(prx, prx)
					fnsr.set(prx, R)
					return prx
				} else if (R instanceof Image && !IMGS.has(R.src)) setImg(R.src)
				//console.info(`APPLY %s(${new Array(args.length).fill('%O').join(', ')}) => %O`, W, ...args, R)
				return R
			},
			construct(or, args) {
				const C = Reflect.construct(...arguments)
				if (W === 'NFBR.a6i.Book') {
					BOOKS.add(C)
					url = args[0]
					const ff = () => {
						let c = C.content[args[1][0] + '_' + args[2][0]]
						if (c === undefined) return requestAnimationFrame(ff)
						bookjson = JSON.stringify(C, null, '\t')
						pages = c.files.length
						dir = (wmr.get(c) || c)['page-progression-direction'] || dir
						GM_notification(`gangan online ripster has ${pages} to download`, 'click to start', null, dlAll)
						nt.mkClickToStart(dlAll)
					}
					requestAnimationFrame(ff)
				}
				if (W === 'NFBR.a6i.Page' && args[1] !== -1) {
					PAGES[args[1]] = args[0]
					setImg(url + args[0])
				}
				if (or.name !== '' ||
					Object.isFrozen(C.prototype) ||
					C instanceof Image ||
					C instanceof HTMLCanvasElement ||
					C instanceof CanvasRenderingContext2D ||
					C instanceof XMLHttpRequest) {
					//console.info(`CONSTRUCT %s <%O>(${new Array(args.length).fill('%O').join(', ')}) => %O`, W, or, ...args, C)
					return C
				}
				try {
					const prx = new Proxy(C, prox('new ', ...w, `(CONSTRUCT#${++constructCount})`))
					//console.info(`CONSTRUCT#%d %s <%O>(${new Array(args.length).fill('%O').join(', ')}) => %O`,
					//			 constructCount, W, or, ...args, C)
					consts.set(C, prx).set(prx, prx)
					constsr.set(prx, C)
					return prx
				} catch(e) {
					console.error(e)
					return C
				}
				// return C
			}
		}
	}
	const g = async () => {
		const {drawImage} = CanvasRenderingContext2D.prototype
		if(undefined === drawImage) throw new TypeError('drawImage bad type')
		NFBR = new Proxy(NFBR, prox('NFBR'))
		CanvasRenderingContext2D.prototype.drawImage = new Proxy(drawImage, {
			apply(fn, self, args) {
				Reflect.apply(fn, self, args)
				let {width, height} = self.canvas
				let h = args[0].src
				args[0] instanceof Image && IMGS.has(args[0].src) && IMGS.get(args[0].src).then(bm => {
					const osc = BMS.get(bm) || BMS.set(bm, new OffscreenCanvas(width, height).getContext('2d')).get(bm)
					args[0] = bm
					osc.drawImage(...args)
					_OSCS.has(osc) || _OSCS.set(osc, setTimeout(() => {
						BLOBS.set(h, osc.canvas.convertToBlob())
						console.log(BLOBS)
					}, 1e3))
				})
			}
		})
		return ['proxy installed on NFBR object', wms]
	}
	g().catch(g).catch(g).then(console.log, e => {console.error(e); throw e})
} catch(e) {
	alert(e.stack)
}