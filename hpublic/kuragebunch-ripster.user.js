// ==UserScript==
// @name         kuragebunch-ripster
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  rips image data from kuragebunch.com/episode/*, pocket.shonenmagazine.com/episode/*, shonenjumpplus.com/episode/*
// @author       zeen3
// @match        https://kuragebunch.com/episode/*
// @match        https://pocket.shonenmagazine.com/episode/*
// @match        https://shonenjumpplus.com/episode/*
// @match        https://tonarinoyj.jp/episode/*
// @match        https://comic-days.com/episode/*
// @grant        GM_notification
// @grant        GM_xmlhttpRequest
// @connect      cdn-img.kuragebunch.com
// @connect      cdn-img.pocket.shonenmagazine.com
// @connect      shonenjumpplus.com
// @connect      cdn-img.tonarinoyj.jp
// @connect      cdn-img.comic-days.com
// @run-at       document-start
// @require      https://zeen3.gitlab.io/ripsters/ripster-shared.js
// @updateURL    https://zeen3.gitlab.io/ripsters/kuragebunch-ripster.js
// ==/UserScript==

(async () => {
	const nt = self[self['next-tick']].suicide();
	const tb = url => new Promise((r,j) => {
		let b = GM_xmlhttpRequest({
			method: 'GET',
			url,
			responseType: 'arraybuffer',
			onerror: j,
			ontimeout: j,
			headers: {
				'User-Agent': 'Mozilla/5.0 Mobile Safari/537.36'
			},
			onload(res) {
				let g = res.responseHeaders.toLowerCase().indexOf('content-type')
				let type = res.responseHeaders.slice(res.responseHeaders.indexOf(' ', g), res.responseHeaders.indexOf('\n', g)).trim()
				r(new Blob([res.response], {type}))
			}
		})
	})
	console.log(nt)
	const _imgs = await new Promise(r => {
		GM_xmlhttpRequest({
			method: 'GET',
			url: location.href,
			headers: {
				'User-Agent': 'Mozilla/5.0 (Linux) Mobile Safari/537.99',
				'Cache-Control': 'no-cache',
				'Pragma': 'no-cache'
			},
			onload(res) {
				const d = new DOMParser();
				let g = d.parseFromString(res.responseText, 'text/html')
				r(g.querySelectorAll([
					'#content .page-area:not([id]) img[src]',
					'#content .page-area:not([id]) img[data-src]',
					'#content .page-area:not([id]) canvas'
				].join(', ')))
			}
		})
	})
	console.log(_imgs)
	await new Promise(r => {
		GM_notification({
			title: 'RIPSter ready',
			text: 'gigaview ripster ready, click to start',
			onclick: r,
			timeout: 0
		})
		console.log({get click_dots_to_start_ripping() {return r()}})
	})
	const {episode} = JSON.parse(document.body.parentElement.dataset.gtmDataLayer)
	const placement = new File(
		[document.body.parentElement.dataset.gtmDataLayer],
		episode.content_id.replace(/[\/\.\\]/g, '_') + '_placement.json',
		{type: 'application/json'}
	)

	const images = []
	// const kr = new KeyboardEvent('keydown', {which: 39})
	// const kl = new KeyboardEvent('keydown', {which: 37})
	// let rtl = document.querySelector('#content .image-container[dir]').dir === 'rtl'
	// const kn = rtl ? kl : kr
	// const kp = rtl ? kr : kl
	const _dl = async () => {
		console.time('__downloadFile__')
		console.log(await nt.__downloadFile__(placement, placement.name))
		console.timeEnd('__downloadFile__')
		let i = 0, r = 0
		while (_imgs.length > i) {
			while (!(images[i] || console.log('%d loops', ++r))) await nt.ric()
			await nt.__downloadFile__(images[i++], episode.content_id + '_' +
									  i.toString(10).padStart(4, '0') + '.' +
									  (await images[i-1]).type.slice(6))
			console.log(i)
		}
		alert(`Completed download of ${i-1} files`)
	}
	let _not = () => {
		_not = nt.noop
		GM_notification({
			title: `Ready to download ${episode.content_id}`,
			text: `Ready to download images partially, click to start downloading.`,
			timeout: 0,
			onclick: _dl
		})
		console.log({get ['download_' + episode.content_id]() {return _dl()}})
	}
	for (const image of _imgs) {
		switch (image.constructor.name) {
			case 'HTMLImageElement': {
				console.time(image.src || image.dataset.src)
				images.push(tb(image.src || image.dataset.src).finally(()=>console.timeEnd(image.src || image.dataset.src)))
				break;
			}
			case 'HTMLCanvasElement': {
				console.log(image)
				images.push(new Promise((r,j) => {try{image.toBlob(r)}catch(e){j(e)}}))
				break;
			}
		}
		_not()
	}
	await Promise.all(images)
	GM_notification({
		title: `Ready to download ${episode.content_id}`,
		text: `${episode.content_id} completed download, click to start.`,
		onclick: _dl,
		timeout: 0
	})
	return Promise.all(images)
})().then(console.log, console.error);
