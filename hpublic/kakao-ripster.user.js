// ==UserScript==
// @name         kakao-ripster
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  rip images from page.kakao.com
// @require      https://zeen3.gitlab.io/ripsters/ripster-shared.js
// @author       zeen3
// @connect      page-edge.kakao.com
// @connect      api2-page.kakao.com
// @match        https://page.kakao.com/*
// @grant        GM_notification
// @grant        GM_xmlhttpRequest
// ==/UserScript==

;(async function downloader (skip = false) {
	const {ntprom, delay, iloadprom, praf, __downloadFile__: dl, nt} = self[self['next-tick']].suicide()
	let prev_search = location.search
	requestIdleCallback(function changingSearch() {
		if (prev_search !== location.search && location.pathname.startsWith('/viewer')) return downloader(skip).then(console.log, console.error)
		else return requestIdleCallback(changingSearch)
	})
    if (!skip && await new Promise(r => {
        GM_notification({
            title: 'RIPSter ready',
            text: `kakao ripster ready to rip ${location.search.slice(11)}, click to start`,
            onclick: r,
            timeout: 0
        })
		nt.mkClickToStart(r)
    })) ;;
	skip = true
	if (prev_search !== location.search) return
    console.log('starting rip')

    let h = [], w = [], is = await fetch(
        'https://api2-page.kakao.com/api/v1/inven/get_download_data/web',
        {method: 'POST', cache: 'reload', body: new URLSearchParams(prev_search)}
    ).then(async load => {
        const resp = await load.json()
        console.log(load, resp)
        let prts = [], total = resp.downloadData.members.totalSize
        return Promise.all(
            Array.from(
                resp.downloadData.members.files,
                (file, i) => new Promise((r, j) => GM_xmlhttpRequest({
					method: 'GET',
                    url: resp.downloadData.members.sAtsServerUrl + file.secureUrl,
                    responseType: 'arraybuffer',
                    headers: {
						Accept: 'image/*',
                        Referer: 'https://page.kakao.com/'
                    },
                    onprogress(pr) {
                        prts[i] = pr
                        let loaded = prts.reduce((a, {loaded = 0} = {}) => a + loaded, 0)
                        console.log('progression: %d/%d (%f%%)', loaded, total, 100 * loaded / total)
                    },
                    onload(_res) {
                        let cr = _res.responseHeaders.toLowerCase().indexOf('content-type')
                        let t = _res.responseHeaders.indexOf(' ', cr)
                        let n = _res.responseHeaders.indexOf('\r\n', t)
                        const type = _res.responseHeaders.slice(t, n).trim()
                        if (!type.startsWith('image')) {
                            console.error(_res)
							console.log(type)
                            return j(_res)
                        }
                        createImageBitmap(new Blob([_res.response], {type})).then(bm => {
                            h[i] = bm.height
                            w[i] = bm.width
                            r(bm)
                            return bm
                        })
                    }
                }))
                )
        )})
    let osc = new OffscreenCanvas(Reflect.apply(Math.max, null, w), h.reduce((a,v)=>a+v, 0)).getContext('2d')
    console.log(osc.canvas, await Promise.all(is))
    let _h = 0
    for await (const bm of is) {
        osc.drawImage(bm, 0, _h)
        _h += bm.height
        bm.close()
    }
    console.log(_h)
    let UI8,
        broken = 0,
        clear = 0,
        H = osc.canvas.height - 511,
        breaks = [osc.canvas.height],
        last = osc.canvas.height + 1,
        ns = 0
    const id = osc.getImageData(0, 0, osc.canvas.width, osc.canvas.height),
          UI8EQ = (v, i) => Math.abs(v - UI8[i & 3]) < 3
    while (--H > 511) {
        UI8 = new Uint8Array(id.data.buffer, id.data.byteOffset + id.width * H << 2, id.width << 2)
        const cb = UI8.every(UI8EQ)
        if (!cb) ++broken
        else if (++clear > 7 && broken && (last - H) > 511) {
			console.log({H, clear, broken, last, ns})
            breaks.push(H)
            last = H
            clear = 0
            broken = 0
            H -= (H & 63)
            ns = 8 // definitely next tick this
        } else if ((last - H) < 480) {
            H -= 63
            ++ns
        }
        if (ns & 7 !== ns || !(H & 63)) await ntprom(ns = 0)
    }
    let breaker = breaks.length
    breaks.push(0)
    const finals = []
    while (--breaker > -1) {
        const H = breaks[breaker] - breaks[breaker + 1],
              X = new OffscreenCanvas(id.width, H).getContext('2d')
        X.drawImage(osc.canvas, 0, breaks[breaker + 1], id.width, H, 0, 0, id.width, H)
        finals.push(X.canvas.convertToBlob())
    }
	const dlAll = async () => {
		await dl(new Blob([JSON.stringify({slices: breaks}, null, '\t')], {type: 'application/json'}), 'placement.json')
		for (let i = 0; i < finals.length;) await dl(finals[i++], i.toString(10).padStart(4, '0').concat('.png'))
	}
	nt.mkClickToStart(dlAll)
	await finals[0]
	GM_notification({
		text: `Re-sliced ${is.length} images as ${finals.length} images; click to start downloading`,
		title: 'RIPster ready for download.',
		timeout: 0,
		onclick: dlAll
	})
    return Promise.all(finals)
})().then(console.log, console.error);
