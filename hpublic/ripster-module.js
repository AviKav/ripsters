import './ripster-shared.js'

const nt = self[self['next-tick']].suicide()

export default nt

const {
	ntc,
	ntp,
	delay,
	iloadprom,
	praf,
	ric,
	__downloadFile__,
	registerGMDL,
	sym,
	noop,
	mkClickToStart
} = nt
export {
	nt,
	ntc, ntc as cancel,
	ntp, ntp as ntprom,
	delay,
	iloadprom,
	praf,
	ric,
	__downloadFile__, __downloadFile__ as dlf,
	registerGMDL, registerGMDL as register_GM_download,
	sym,
	noop,
	mkClickToStartm
}
