// ==UserScript==
// @name         x-ripster-shared
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  utils to aid in ripping images
// @author       zeen3
// ==/UserScript==

;(()=>{
	const A = new Promise(r => {
		try {
			r(document.createElement('a'))
		} catch (e) {
			addEventListener('load', e => {
				try {
					r(e.target.createElement('a'))
				} catch (x) {
					r(document.createElement('a'))
				}
			}, {once: true})
		}
	})
	let k = 0,
		clickDL = async (blob, filename, headers = {}) => {
			const a = await A
			let clean = false
			a.href = 'string' === typeof blob
				? blob
				: (clean = URL.createObjectURL(blob))
			a.download = filename
			await ntp()
			a.click()
			await praf()
			if (clean) URL.revokeObjectURL(clean)
			a.remove()
			await praf()
		},
		_dl = clickDL
	// messagechannel is async, but yielding (task, not microtask).
	const {port1, port2} = new MessageChannel,
		FNs = new Array(1), // alignment
		ARGs = new Array(1), // alignment
		sy = Symbol('next-tick'),
		noop = async () => {},
		noopArray = Object.freeze([]),
		__ntp = res => {FNs.push(() => res()); ARGs.push(noopArray); port2.postMessage(++k)},
		_ntp = () => new Promise(__ntp),
		ntp = () => Promise.resolve().then(_ntp),
		nt = (fn, ...args) => 'function' === typeof nt
			? (FNs.push(fn), ARGs.push(args), port2.postMessage(++k), k)
			: ntp(),
		ntc = Nt => (delete FNs[Nt], delete ARGs[Nt], void 0),
		delay = d => new Promise(r => setTimeout(r, d)),
		iloadprom = l => new Promise(
			r => l.complete
			? nt(r)
			: l.addEventListener('load', () => r())
		),
		_praf = res => requestAnimationFrame(res),
		praf = () => new Promise(_praf),
		_pric = res => requestIdleCallback(res),
		ric = () => new Promise(_pric),
		__downloadFile__ = async (blob, name = blob.name, headers, args) => {
			let filename = prompt(`Downloading file with given name, clear to stop downloading; is this all right?`, (await blob, await name))
			if (!filename)
				throw new RangeError('Unable to download without user agreement')
			await _dl(await blob, await filename, await headers, await args)
			return true
		},
		registerGMDL = GMDL => {
			if ('function' !== typeof GMDL) throw new TypeError(typeof GMDL)
			_dl = (file, name = file.name, headers = {}, args = {}, cancel) => new Promise((r, j) => {
				if (file instanceof Blob || file instanceof File ||
					('string' === typeof file && file.slice(0,5) === 'data:'))
					r(clickDL(file, name))
				else if ('string' === typeof file) {
					
					let g = GMDL({
						headers,
						onload: r,
						onerror: j,
						ontimeout: j,
						url: file,
						method: 'GET',
						saveAs: true,
						name,
						...args,
					})
					cancel && (cancel.onabort = g.abort)
				} else nt(() => j(file))
			})
			return true
		}

	port1.onmessage = ({data}) => {
		if ('function' === typeof FNs[data] && Array.isArray(ARGs[data]))
			Reflect.apply(FNs[data], null, ARGs[data])
		ntc(data)
	}

	nt.nt = nt
	nt.cancel = ntc
	nt.ntc = ntc
	nt.ntprom = ntp
	nt.ntp = ntp
	nt.delay = delay
	nt.iloadprom = iloadprom
	nt.praf = praf
	nt.ric = ric
	nt.__downloadFile__ = __downloadFile__
	nt.registerGMDL = registerGMDL
	nt.register_GM_download = registerGMDL

	nt.sym = sy
	nt.noop = noop // literally
	// yep
	nt.suicide = () => (
		Reflect.deleteProperty(window, 'next-tick'),
		Reflect.deleteProperty(window, sy),
		nt
	)
	function OpenMeUpToDownload(cb, name = 'click_to_start') {
		Object.defineProperty(this, name, {
			get: cb,
			enumerable: true,
			configurable: true
		})
		return this
	}
	const Downloadable = OpenMeUpToDownload
	nt.mkClickToStart = (cb, name) => {
		const o = new Downloadable(cb, name)
		nt(console.dir, o)
		return o
	}
	Object.freeze(nt) // prevent modifications
	Object.defineProperty(window, sy, {get() {return nt}, configurable: true})
	Object.defineProperty(window, 'next-tick', {value: sy, configurable: true})
})();
